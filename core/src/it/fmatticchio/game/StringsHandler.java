package it.fmatticchio.game;

/**
 * Handle the strings
 */
public class StringsHandler {
    private static String[] strings = {
            "YES",//0
            "NO",//1
            "OK",//2
            "START",//3
            "PAUSE",//4
            "GAMEOVER",//5
            "HIGHSCORE",//6
            "NEW HIGHSCORE",//7
            "Do you really want to quit?",//8
            "Sign in",//9
            "Sign out",//10
            "Log in failed",//11
            "You are currently signed in to Google Play Games",//12
            "You are currently signed out of Google Play Games",//13
            "Start over?"//14
    };

    private static String[] achievement = {
            "100",//0
            "500",//1
            "jumps",//2
            "shots",//3
            "1000",//4
            "Around the World"//5
    };
    public static final int
            //Negative indexes are for dialogs which handles their own strings, like the Google Play Games Dialog
            GPGDIALOG = -1,
            OPTIONS = -2,
            SHARE = -3,

            YES = 0,
            NO = 1,
            OK = 2,
            START = 3,
            PAUSE = 4,
            GAMEOVER = 5,
            HIGHSCORE = 6,
            NEWHS = 7,
            QUIT = 8,
            SIGNIN = 9,
            SIGNOUT = 10,
            LOGINFAIL = 11,
            ISSIGNIN = 12,
            ISSIGNOUT = 13,
            RESTART = 14,

            ACH100 = 0,
            ACH500 = 1,
            ACHJUMPS = 2,
            ACHSHOTS = 3,
            ACH1000 = 4,
            ACHATW = 5;

    public static void setStrings(String[] s){
        strings = s;
    }
    public static void setAchievement(String[] s){ achievement = s; }

    public static String getString(int i){
        String ret = "INVALID INDEX";
        if(i < strings.length) ret = strings[i];

        return ret;
    }

    public static String getAchievement( int i){
        String ret = "INVALID INDEX";
        if(i < achievement.length) ret = achievement[i];

        return ret;
    }
}
