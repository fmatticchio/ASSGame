package it.fmatticchio.game;
import com.badlogic.gdx.Gdx;



/**
 * Dummy implementation of Google Play Services for the desktop version
 */
public class DesktopGS implements IGoogleServices {
    private boolean signedIn;
    private static final String CLASS = "Google services";
    @Override
    public void signIn() {
        Gdx.app.log(CLASS , "sign in");
        signedIn = true;
    }

    @Override
    public void signOut() {
        Gdx.app.log(CLASS , "sign out");
        signedIn = false;
    }

    @Override
    public void shareGame() {
        Gdx.app.log(CLASS , "share");
    }

    @Override
    public void rateGame() {Gdx.app.log(CLASS , "rate");}

    @Override
    public void shareScore(long score) {
        Gdx.app.log(CLASS , String.format("share %d score ", score));
    }

    @Override
    public void submitScore(long score) {
        Gdx.app.log(CLASS , "score: "+score);
    }

    @Override
    public void showScores() {
        Gdx.app.log(CLASS , "scores");
    }

    @Override
    public void showAchievement() {
        Gdx.app.log(CLASS , "achievements");
    }

    @Override
    public void unlock(String achievement){
        Gdx.app.log(CLASS, "unlock " + achievement);
    }

    @Override
    public boolean isSignedIn() {
        Gdx.app.log(CLASS, "is signed in? " + signedIn);
        return signedIn;
    }
}
