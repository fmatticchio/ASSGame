package it.fmatticchio.game;

import com.badlogic.gdx.Game;

import it.fmatticchio.helpers.AchievementsHandler;
import it.fmatticchio.helpers.AssetsLoader;
import it.fmatticchio.screen.GameScreen;

public class ASSGame extends Game {
	private static IGoogleServices googleServices;

	public ASSGame(IGoogleServices googleServices){
		super();
		ASSGame.googleServices = googleServices;
	}

	@Override
	public void create () {
		AssetsLoader.load();
        AchievementsHandler.load(googleServices);
		setScreen(new GameScreen(this));
	}

	@Override
	public void dispose(){
		super.dispose();
        screen.dispose();
		AssetsLoader.dispose();
        AchievementsHandler.save();
	}

    public IGoogleServices getServices(){
        return googleServices;
    }

}
