package it.fmatticchio.game;
/**
 * Interface to implement google play services
 */
public interface IGoogleServices {
    public void signIn();
    public void signOut();
    public void shareGame();
    public void rateGame();
    public void shareScore(long score);
    public void submitScore(long score);
    public void showScores();
    public void showAchievement();
    public void unlock(String achievement);
    public boolean isSignedIn();
}
