package it.fmatticchio.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import it.fmatticchio.game.IGoogleServices;
import it.fmatticchio.game.StringsHandler;
import it.fmatticchio.gameworld.GameRenderer;

/**
 * Handle the achievements
 */
public class AchievementsHandler {
    private static final String JUMPS = "jumps" , SHOTS = "shots" , MASK = "mask";

    private static final int
            ACH100 = 1,
            ACH500 = 2,
            ACHJUMPS = 4,
            ACHSHOTS = 8,
            ACH1000 = 16,
            ACHATW = 32,

            MIRROR = 1,
            UPSIDEDOWN = 2,
            R180 = 4;

    private static IGoogleServices gs;

    private static int jumps, shots, mask, maskOrientation;

    private static Preferences preferences;


    public static void load(IGoogleServices gs) {
        AchievementsHandler.gs = gs;
        preferences = Gdx.app.getPreferences("Achievement");
        jumps = preferences.getInteger(JUMPS, 0);
        shots = preferences.getInteger(SHOTS, 0);
        mask = preferences.getInteger(MASK, 0);
        maskOrientation = 0;
    }

    public static void save(){
        preferences.putInteger(JUMPS , jumps);
        preferences.putInteger(SHOTS , shots);
        preferences.putInteger(MASK , mask);
        preferences.flush();
    }

    public static void onGameOver(int score){
        if((mask & ACH100) != ACH100  && score >= 100) {
            gs.unlock(StringsHandler.getAchievement(StringsHandler.ACH100));
            mask |= ACH100;
        }

        if((mask & ACH500) != ACH500  && score >= 500) {
            gs.unlock(StringsHandler.getAchievement(StringsHandler.ACH500));
            mask |= ACH500;
        }
        if((mask & ACHJUMPS) != ACHJUMPS  && jumps >= 1000) {
            gs.unlock(StringsHandler.getAchievement(StringsHandler.ACHJUMPS));
            mask |= ACHJUMPS;
        }
        if((mask & ACH500) != ACH500  && shots >= 5000) {
            gs.unlock(StringsHandler.getAchievement(StringsHandler.ACH500));
            mask |= ACH500;
        }

        if((mask & ACH1000) != ACH1000  && score >= 1000) {
            gs.unlock(StringsHandler.getAchievement(StringsHandler.ACH1000));
            mask |= ACH1000;
        }

        if((mask & ACHATW) != ACHATW && maskOrientation == (MIRROR|UPSIDEDOWN|R180)){
            gs.unlock(StringsHandler.getAchievement(StringsHandler.ACHATW));
            mask |= ACHATW;
        }

        maskOrientation = 0;

        save();
    }

    public static void sync(){
        if((mask & ACH100) == ACH100) gs.unlock(StringsHandler.getAchievement(StringsHandler.ACH100));
        if((mask & ACH500) == ACH500) gs.unlock(StringsHandler.getAchievement(StringsHandler.ACH500));
        if((mask & ACHJUMPS) == ACHJUMPS) gs.unlock(StringsHandler.getAchievement(StringsHandler.ACHJUMPS));
        if((mask & ACHSHOTS) == ACHSHOTS) gs.unlock(StringsHandler.getAchievement(StringsHandler.ACHSHOTS));
        if((mask & ACH1000) == ACH1000) gs.unlock(StringsHandler.getAchievement(StringsHandler.ACH1000));
    }

    public static void addJump(){
        jumps++;
    }

    public static void addShot(){
        shots++;
    }

    public static void addOrientation(GameRenderer.Orientation o) {
        switch(o){

            case NORMAL:
                break;
            case UPSIDEDOWN:
                maskOrientation |= UPSIDEDOWN;
                break;
            case MIRROR:
                maskOrientation |= MIRROR;
                break;
            case R180:
                maskOrientation |= R180;
                break;
        }
    }
}
