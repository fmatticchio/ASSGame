package it.fmatticchio.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Rectangle;

import it.fmatticchio.game.StringsHandler;
import it.fmatticchio.gameobjects.Player;
import it.fmatticchio.gameworld.GameRenderer;
import it.fmatticchio.gameworld.GameWorld;
import it.fmatticchio.screen.GameScreen;
import it.fmatticchio.ui.GPGDialog;
import it.fmatticchio.ui.Options;
import it.fmatticchio.ui.ShareDialog;
import it.fmatticchio.ui.YNDialog;

/**
 * Input Handler class
 */
public class InputHandler implements InputProcessor {
    private GameWorld world;
    private Player hero;

    private float scaleX, scaleY;

    public InputHandler(GameWorld world, float scaleX, float scaleY){
        this.world = world;
        hero = world.getPlayer();

        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.Q || keycode == Input.Keys.BACK) {
            if (world.getCurrentState() == GameWorld.GameState.RUNNING) {
                world.pause();
            } else if (!world.isDialogOn()) {
                world.openDialog(StringsHandler.QUIT);
            } else world.resume();
            return true;
        }

        if(keycode == Input.Keys.O){
            world.openDialog(StringsHandler.OPTIONS);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if(world.isDialogOn()) {
            return handleDialog(screenX,screenY);
        }else {

            /*if (AssetsLoader.volumeButton.isClicked(screenX / scaleX, screenY / scaleY)) {
                AssetsLoader.volumeButton.click();
                AssetsLoader.setVolume(AssetsLoader.volumeButton.isOn() ? 1 : 0);
                return true;
            }*/

            if (!AssetsLoader.playButton.isOn() && AssetsLoader.playButton.isClicked(screenX / scaleX, screenY / scaleY)) {
                world.pause();
                return true;
            }

            if (world.getCurrentState() != GameWorld.GameState.RUNNING) {
                if (AssetsLoader.gPlayLogIn.isClicked(screenX / scaleX, screenY / scaleY)) {
                    world.openDialog(StringsHandler.GPGDIALOG);
                    return true;
                }

                if (AssetsLoader.volume.isClicked(screenX / scaleX, screenY / scaleY)) {
                    world.openDialog(StringsHandler.OPTIONS);
                    return true;
                }

                if(GameRenderer.SHAREICON.contains(screenX / scaleX, GameScreen.SCREENY -  screenY / scaleY)){
                    world.openDialog(StringsHandler.SHARE);
                    //world.getGame().getServices().shareGame();
                    return true;
                }

                /*if (AssetsLoader.gPlayLeaderboards.isClicked(screenX / scaleX, screenY / scaleY)) {
                    world.getGame().getServices().showScores();
                    return true;
                }

                if (AssetsLoader.gPlayAchievement.isClicked(screenX / scaleX, screenY / scaleY)) {
                    world.getGame().getServices().showAchievement();
                    return true;
                }*/


            }else {
                if (AssetsLoader.reset.isClicked(screenX / scaleX, screenY / scaleY)) {
                    world.openDialog(StringsHandler.RESTART);
                    return true;
                }
            }


            switch (world.getCurrentState()) {

                case GAMEOVER:
                    world.reset();
                case START:
                case PAUSE:
                    world.start();
                case RUNNING:
                    switch (world.getRenderer().getOrientation()) {
                        case NORMAL:
                        case UPSIDEDOWN:
                            if (screenX < scaleX * GameScreen.SCREENX / 2) {
                                hero.setJump(true);
                            } else hero.setShoot(true);
                            break;
                        case MIRROR:
                        case R180:
                            if (screenX > scaleX * GameScreen.SCREENX / 2) {
                                hero.setJump(true);
                            } else hero.setShoot(true);
                            break;
                    }
                    break;
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(world.getCurrentState() == GameWorld.GameState.RUNNING){
            hero.setJump(false);
            hero.setShoot(false);
        }
        if(world.getDialog() == StringsHandler.OPTIONS && Options.isDragging()) {
            Options.setDragging(false);
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(world.getDialog() == StringsHandler.OPTIONS && Options.isDragging()) {
            Options.drag(screenX / scaleX, GameScreen.SCREENY - screenY / scaleY);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    private boolean handleDialog(int screenX, int screenY) {
        if(world.getDialog() == StringsHandler.GPGDIALOG) {
            switch (GPGDialog.click(screenX / scaleX, GameScreen.SCREENY - screenY / scaleY)){
                case ACHIEVEMENTS:
                    if (world.getGame().getServices().isSignedIn())
                        world.getGame().getServices().showAchievement();
                    break;
                case LEADERBOARDS:
                    if (world.getGame().getServices().isSignedIn())
                        world.getGame().getServices().showScores();
                    break;
                case SIGNIN:
                    if (world.getGame().getServices().isSignedIn())
                        world.getGame().getServices().signOut();
                    else world.getGame().getServices().signIn();
                    break;
                case OUT:
                    world.resume();
                    break;
                case NULL:
                    break;
            }
        }else if(world.getDialog() == StringsHandler.OPTIONS) {
            switch (Options.click(screenX / scaleX, GameScreen.SCREENY - screenY / scaleY)){

                case SLIDER:
                    Options.setDragging(true);
                    break;
                case VOLUME:
                    Options.setDragging(true);
                    break;
                case OUT:
                    world.resume();
                    break;
                case NULL:
                    break;
            }
        }else if(world.getDialog() == StringsHandler.SHARE) {
            switch (ShareDialog.click(screenX / scaleX, GameScreen.SCREENY - screenY / scaleY)) {

                case SHARE:
                    world.getGame().getServices().shareGame();
                    break;
                case RATE:
                    world.getGame().getServices().rateGame();
                    break;
                case SCORE:
                    world.getGame().getServices().shareScore(world.getScore());
                    break;
                case OUT:
                    world.resume();
                    break;
                case NULL:
                    break;
            }
        }else{
            switch (YNDialog.click(screenX / scaleX, GameScreen.SCREENY - screenY / scaleY)) {
                case YES:
                    switch (world.getDialog()) {
                        case StringsHandler.QUIT:
                            Gdx.app.exit();
                            break;
                        case StringsHandler.SIGNIN:
                            world.getGame().getServices().signIn();
                            world.resume();
                            break;
                        case StringsHandler.SIGNOUT:
                            world.getGame().getServices().signOut();
                            world.resume();
                            break;
                        case StringsHandler.RESTART:
                            world.reset();
                            world.resume();
                            break;
                    }

                case NO:case OUT:
                    world.resume();
                    break;
                case NULL:
                    break;
            }
        }

        return true;

    }

}
