package it.fmatticchio.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import it.fmatticchio.screen.GameScreen;
import it.fmatticchio.ui.CheckButton;
import it.fmatticchio.ui.CustomIcon;

/**
 * Asset Loader Class
 */
public class AssetsLoader {
    private static String HIGHSCORE = "highScore" , PREFERENCE = "ASS" , VOLUME = "VOLUME";
    public static Sound hit, hitWall, coin, shot, collision, jump;

    private static Texture texture;
    private static TextureRegion volumeOn, volumeOff, pause, play, logIn , leaderboards , achievement , heartTex , resetTex;
    public static TextureRegion floor1 , floor2;
    public static CheckButton playButton;

    public static CustomIcon gPlayLogIn, volume , reset , heart ;//, gPlayLeaderboards, gPlayAchievement;


    private static Preferences preferences;



    public static void load(){

        hit = Gdx.audio.newSound(Gdx.files.internal("data/enemyHit.wav"));
        hitWall = Gdx.audio.newSound(Gdx.files.internal("data/wallHit.wav"));
        coin = Gdx.audio.newSound(Gdx.files.internal("data/coin.wav"));
        shot = Gdx.audio.newSound(Gdx.files.internal("data/shot.wav"));
        collision = Gdx.audio.newSound(Gdx.files.internal("data/playerCollide.wav"));
        jump = Gdx.audio.newSound(Gdx.files.internal("data/playerJump.wav"));

        texture = new Texture(Gdx.files.internal("data/texture.png"));
        texture.setFilter(Texture.TextureFilter.Nearest , Texture.TextureFilter.Nearest);



        preferences = Gdx.app.getPreferences(PREFERENCE);
        if (!preferences.contains(HIGHSCORE)){
            preferences.putInteger(HIGHSCORE , 0);
        }
        if (!preferences.contains(VOLUME)){
            preferences.putFloat(VOLUME , 1);
        }

        floor1 = new TextureRegion(texture, 0, 256, 256, 128);
        floor2 = new TextureRegion(floor1);
        floor2.flip(true , false);

        volumeOn = new TextureRegion(texture, 0, 128 ,128 ,128);
        volumeOff = new TextureRegion(texture, 128, 128 ,128 ,128);
        play = new TextureRegion(texture , 0,0 , 128, 128);
        pause = new TextureRegion(texture, 128 , 0 , 128 , 128);

        logIn = new TextureRegion(texture, 256 ,128 , 128 , 128 );
        leaderboards = new TextureRegion(texture, 384 , 0, 128 , 128 );
        achievement = new TextureRegion(texture, 256 , 0 , 128 , 128 );

        resetTex = new TextureRegion(texture,384 , 128 ,128,128 );

        heartTex = new TextureRegion(texture , 0 , 384 , 128 , 128);


        //volumeButton = new CheckButton(GameScreen.SCREENX - 45 , GameScreen.SCREENY - 30 , 15 , 15 , volumeOn , volumeOff , preferences.getFloat(VOLUME) == 1);
        playButton = new CheckButton(GameScreen.SCREENX - 25 , GameScreen.SCREENY - 30 , 15 , 15 , play , pause );

        gPlayLogIn = new CustomIcon(GameScreen.SCREENX/2f - 40 , 50 , 20 , 20 , logIn);
        //gPlayAchievement = new CustomIcon(GameScreen.SCREENX/2f + 20 , 50 , 20 , 20 , achievement);
        //gPlayLeaderboards = new CustomIcon(GameScreen.SCREENX/2f - 10 , 50 , 20 , 20 , leaderboards);

        volume = new CustomIcon(GameScreen.SCREENX/2f - 10  , 50, 20 , 20 , volumeOn );

        reset = new CustomIcon(GameScreen.SCREENX - 55 , GameScreen.SCREENY - 30 , 15 , 15 , resetTex);

        heart = new CustomIcon( 0 , 0 , 20 ,20 , heartTex);

    }

    public static void setHighScore(int score){
        preferences.putInteger(HIGHSCORE , score);
        preferences.flush();
    }

    public static int getHighScore(){
        return preferences.getInteger(HIGHSCORE);
    }

    public static float volume(){
        return preferences.getFloat(VOLUME);
    }

    public static void setVolume(float volume){
        preferences.putFloat(VOLUME, volume);
        preferences.flush();
    }

    public static void dispose(){
        hit.dispose();
        hitWall.dispose();
        coin.dispose();
        shot.dispose();
        texture.dispose();
    }
}
