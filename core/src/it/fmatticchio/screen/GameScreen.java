package it.fmatticchio.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import it.fmatticchio.helpers.InputHandler;
import it.fmatticchio.game.ASSGame;
import it.fmatticchio.gameworld.GameWorld;
import it.fmatticchio.gameworld.GameRenderer;

/**
 * Created by francesco on 19/01/16.
 */
public class GameScreen implements Screen {
    public static final int SCREENX = 408, SCREENY = 272;

    private GameWorld world;
    private GameRenderer renderer;
    private float runTime;

    public GameScreen(ASSGame game){
        world = GameWorld.getWorld();
        renderer = new GameRenderer(world,Gdx.graphics.getWidth()/(SCREENX+0f),Gdx.graphics.getHeight()/(SCREENY+0f));
        world.setRenderer(renderer);
        world.setGame(game);
        Gdx.input.setInputProcessor(new InputHandler(world,Gdx.graphics.getWidth()/(SCREENX + 0f),Gdx.graphics.getHeight()/(SCREENY+0f)));
    }

    @Override
    public void render(float delta) {
        runTime += delta;
        world.update(delta);
        renderer.render(delta, runTime);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        renderer.dispose();
    }
}
