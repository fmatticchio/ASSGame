package it.fmatticchio.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import it.fmatticchio.gameworld.GameRenderer;
import it.fmatticchio.gameworld.GameWorld;
import it.fmatticchio.helpers.AssetsLoader;

/**
 * Created by francesco on 19/01/16.
 */
public class Coin extends Scrollable {

    private Ellipse bound;
    private float startWidth;
    private boolean growing = false;

    public Coin(float x, float y, float width, float height, float scrollSpeed) {
        super(x, y, width, height, scrollSpeed);
        bound = new Ellipse(x,y,width,height);
        startWidth = width;
    }

    @Override
    public void update(float delta){
        super.update(delta);
        if(growing){
            width-=velocity.x*delta/2;
            if (width > startWidth) growing = false;
        }else{
            width+=velocity.x*delta/2;
            if (width < 0 ) growing = true;
        }
    }

    @Override
    public boolean collides(Player p){
        Rectangle temp = new Rectangle(getX(),getY(),getWidth(),getHeight());
        boolean r = Intersector.overlaps(temp , p.getBound());
        if(r) {
            AssetsLoader.coin.play(AssetsLoader.volume());
            p.collectBonus();
            die();
        }
        return r;
    }

    @Override
    public boolean collides(Projectile p){
        boolean r =  bound.contains(p.getPosition());
        //Do nothing;
        return r;
    }

    @Override
    public Ellipse getBound(){
        return bound;
    }

    @Override
    public void draw(GameRenderer renderer) {
        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Line);
        renderer.getShapeRenderer().setColor(Color.GOLD);

        // I have no idea why I have to do this, ShapeRenderer.ellipse doesn't like both width and height negative.
        if(renderer.getOrientation() == GameRenderer.Orientation.R180){
            renderer.getShapeRenderer().ellipse(renderer.getOrientation().getOx() - getX() - getWidth(), renderer.getOrientation().getOy() - getY() - getHeight(), getWidth(), getHeight());
        }else{
            renderer.getShapeRenderer().ellipse(renderer.getOrientation().getOx() + renderer.getOrientation().getDx() * getX(), renderer.getOrientation().getOy() + renderer.getOrientation().getDy() * getY(), renderer.getOrientation().getDx() * getWidth(), renderer.getOrientation().getDy() * getHeight());
        }

        renderer.getShapeRenderer().end();

        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        renderer.getShapeRenderer().setColor(Color.YELLOW);

        if(renderer.getOrientation() == GameRenderer.Orientation.R180){
            renderer.getShapeRenderer().ellipse(renderer.getOrientation().getOx() - getX() - getWidth(), renderer.getOrientation().getOy() - getY() - getHeight(), getWidth(), getHeight());
        }else{
            renderer.getShapeRenderer().ellipse(renderer.getOrientation().getOx() + renderer.getOrientation().getDx() * getX(), renderer.getOrientation().getOy() + renderer.getOrientation().getDy() * getY(), renderer.getOrientation().getDx() * getWidth(), renderer.getOrientation().getDy() * getHeight());
        }

        renderer.getShapeRenderer().end();
    }
}
