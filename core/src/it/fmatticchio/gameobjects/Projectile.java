package it.fmatticchio.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import it.fmatticchio.gameworld.GameRenderer;
import it.fmatticchio.screen.GameScreen;

/**
 * Created by francesco on 19/01/16.
 */
public class Projectile implements Drawable{
    public static final int VELOCITY = 350;

    private Vector2 position;
    private boolean isVisible;

    public Projectile(Vector2 start){
        position = start.cpy();
        isVisible = true;
        position.y+=5;
    }

    public void update(float delta){
        position.x+= VELOCITY*delta;
        if(position.x > GameScreen.SCREENX) {
            isVisible = false;
        }
    }

    public Vector2 getPosition(){
        return position;
    }

    public  boolean isVisible(){
        return isVisible;
    }
    public void destroy(){
        isVisible = false;
    }

    @Override
    public void draw(GameRenderer renderer) {
        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        renderer.getShapeRenderer().setColor(Color.YELLOW);
        renderer.getShapeRenderer().circle(renderer.getOrientation().getOx() + renderer.getOrientation().getDx() * position.x, renderer.getOrientation().getOy() + renderer.getOrientation().getDy() * position.y, 2.5f);

        renderer.getShapeRenderer().end();
    }
}
