package it.fmatticchio.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import com.badlogic.gdx.math.Vector2;
import it.fmatticchio.gameworld.GameRenderer;
import it.fmatticchio.gameworld.GameWorld;
import it.fmatticchio.helpers.AchievementsHandler;
import it.fmatticchio.helpers.AssetsLoader;
import it.fmatticchio.screen.GameScreen;

import java.util.ArrayList;

/**
 * The player class
 */
public class Player implements Drawable{
    public static final int STARTINGSIDES = 3 ,  STARTINGLIVES = 3, MAXPROJECTILES = 20;
    public static final float SHOTPAUSE = .3f , INVINCIBLE = 3f , JUMPVELOCITYINIT = 650 , ACCELERATIONINIT = -2000;

    private static Pixmap PIX = new Pixmap(1,1, Pixmap.Format.RGBA4444);
    static{
        PIX.setColor(Color.RED);
        PIX.fill();
    }
    private static Texture TEX = new Texture(PIX);
    private static TextureRegion TEXREG = new TextureRegion(TEX);
    private static EarClippingTriangulator TRIANGULATOR = new EarClippingTriangulator();
    public static void dispose(){
        PIX.dispose();
        TEX.dispose();
    }

    public int verticesNum, numProjectiles;

    private static final Color MORTAL = new Color(0f,0f,1f,.5f), IMMORTAL = new Color(Color.GOLD);

    private GameWorld world;

    private Vector2 position, velocity, acceleration;
    private Vector2[] vertices;
    private float shootingTime, invincibleTime, radius ,bottom, realCenter , rotateVelocity, jumpVelocity ;

    private float theta;
    private float deadRotation = 0;

    private int lives;

    private Rectangle bound;

    private ArrayList<Projectile> projectiles;

    private boolean jumping, jump;
    private boolean shooting, shoot;
    private boolean invincible;
    private boolean alive;


    public Color color, colorT;

    public Player(float x, float y, float radius, GameWorld world){
        realCenter = GameWorld.BASELINE;
        jumping = y>realCenter;
        verticesNum = STARTINGSIDES;

        position = new Vector2(x,y);
        jumpVelocity = JUMPVELOCITYINIT;
        this.radius = radius;
        theta = 0;

        numProjectiles = 10;

        this.world = world;

        bottom = realCenter+(radius*MathUtils.cos(2*MathUtils.PI / verticesNum));


        lives = STARTINGLIVES;
        vertices = new Vector2[verticesNum];

        createVertices();

        velocity = new Vector2(0,0);
        acceleration = new Vector2(0, ACCELERATIONINIT);
        rotateVelocity =  2 * MathUtils.PI / (verticesNum *(-2*jumpVelocity/acceleration.y));

        invincible = false;
        shooting = false;
        jump = false;
        shoot = false;
        projectiles = new ArrayList<Projectile>();

        alive = true;

        color = MORTAL;
        colorT = new Color(MathUtils.random(0.5f,1f), MathUtils.random(0.5f,1f), MathUtils.random(.5f), MathUtils.random(.5f,1f));
        resetColor(colorT);

    }

    private static void resetColor(Color color){
        PIX.setColor(color);
        PIX.fill();
        TEX.dispose();
        TEX = new Texture(PIX);
        TEXREG.setTexture(TEX);
    }

    public void restart(float x, float y, float radius) {
        realCenter = GameWorld.BASELINE;
        jumping = y > realCenter;
        verticesNum = STARTINGSIDES;
        jumpVelocity = JUMPVELOCITYINIT;
        acceleration.y = ACCELERATIONINIT;
        rotateVelocity =  2 * MathUtils.PI / (verticesNum *(-2*jumpVelocity/acceleration.y));

        velocity.y = 0;

        position.x = x;
        position.y = y;

        vertices = new Vector2[verticesNum];
        this.radius = radius;
        createVertices();
        invincible = false;
        shooting = false;
        jump = false;
        shoot = false;
        deadRotation = 0;

        numProjectiles = 10;

        color = MORTAL;
        colorT = new Color(MathUtils.random(0.5f,1f), MathUtils.random(0.5f,1f), MathUtils.random(.5f), MathUtils.random(.5f,1f));
        resetColor(colorT);

        lives = STARTINGLIVES;

        alive = true;
    }

    private void createVertices(){
        theta += ((verticesNum+1)%2)*MathUtils.PI/verticesNum;
        float angle = 2*MathUtils.PI / verticesNum;
        int i;
        for (i = 0; i < verticesNum; i++){
            vertices[i]= new Vector2(position.x+(radius*MathUtils.sin(i*angle + theta)),position.y+(radius*MathUtils.cos(i*angle + theta)));
        }
        bound = (new Polygon(getVertices())).getBoundingRectangle();

    }

    private void computeVertices() {
        float angle = 2*MathUtils.PI / verticesNum;
        int i;
        for (i = 0; i < verticesNum; i++){
            vertices[i].x=position.x+(radius*MathUtils.sin(i*angle + theta));
            vertices[i].y=position.y+(radius*MathUtils.cos(i*angle + theta));
        }
        bound = (new Polygon(getVertices())).getBoundingRectangle();
    }

    public void update(float delta){

        switch (world.getCurrentState()){
            case RUNNING:
                if (alive) updateAlive(delta);
                else updateDead(delta);
                break;
            case GAMEOVER:
                break;
            case START:
                rotate(delta*MathUtils.PI/3);
                computeVertices();
        }


    }

    private void updateAlive(float delta){
        if(jump && !jumping) jump();
        if(shoot && !shooting) shoot();

        velocity.add(acceleration.cpy().scl(delta));
        position.add(velocity.cpy().scl(delta));

        if (jumping) {
            rotate(rotateVelocity *delta);
        }

        if(position.y < realCenter){
            velocity.y = 0;
            position.y = realCenter;
            theta =  ((verticesNum+1)%2)*MathUtils.PI/verticesNum;
            jumping = false;
        }
        computeVertices();

        if (shooting){
            shootingTime += delta;
            shooting = shootingTime < SHOTPAUSE;
        }

        if(invincible){
            invincibleTime+= delta;
            if(invincibleTime > INVINCIBLE) {
                invincible = false;
                color = MORTAL;
            }
        }


    }

    private void updateDead(float delta){
        if(radius > 0) {
            if (position.y < (GameScreen.SCREENY+ realCenter)/2) {
                position.y += delta *150;
            } else {
                radius -=delta*10;
                if(radius < 0) {
                    radius = 0;
                    world.gameOver();
                }
            }
            if (deadRotation < 25) {
                deadRotation += 4*delta;
            }
            rotate(deadRotation * delta);
            computeVertices();
            color.set(MathUtils.random(1f), MathUtils.random(1f), MathUtils.random(1f), MathUtils.random(1f));
            colorT.set(MathUtils.random(1f), MathUtils.random(1f), MathUtils.random(1f), MathUtils.random(1f));
            resetColor(colorT);
        }
    }

    public void jump(){
        if (!jumping) {
            AchievementsHandler.addJump();
            jumping = true;
            AssetsLoader.jump.play(AssetsLoader.volume());
            velocity.y = jumpVelocity;
        }
    }




    public void shoot() {
        if(numProjectiles > 0 && !shooting && !jumping) {
            //Gdx.app.log("shot", "bang!");
            AchievementsHandler.addShot();
            projectiles.add(new Projectile(position));
            numProjectiles--;
            AssetsLoader.shot.play(AssetsLoader.volume());
            shootingTime = 0;
            shooting = true;
           // Gdx.app.log("Added" , projectiles.size() +"");
        }
    }

    private void rotate(float alpha){
        theta+=alpha;
        if (theta > 2*MathUtils.PI) theta -= 2*MathUtils.PI;

        computeVertices();

    }

    public boolean collidesTop(Scrollable s){
        Rectangle temp = new Rectangle(s.getX(),s.getY()+s.getHeight()/2,s.getWidth(),s.getHeight()/2);
        return  Intersector.overlaps(temp, bound);
    }

    public boolean collidesSide(Scrollable s){
        Rectangle temp = new Rectangle(s.getX(),s.getY(),1,s.getHeight()/3);
        return  Intersector.overlaps(temp, bound);
    }

    public int collectBonus(){
        float rand = MathUtils.random(1f);
        int points = 0;
        if(rand < .05) lives++;
        else if (rand > .05 && rand < .10) {
            color = IMMORTAL;
            invincible = true;
            invincibleTime = -5;
        }
        else if (rand > .10 && rand < .20){
            points = 50;
        }
        else if( rand > .20 && rand < .60){
            world.getRenderer().setOrientation(GameRenderer.Orientation.random());
            invincibleTime = -1; //Twofold: first it gives a little bonus if, by chance, the orientation didn't change, second it avoids that the player dies in the confusion following the change of orientation
        }
        else if( rand > .60 && rand < .80){
            /**************************************
            **The change of the number of vertices*
            **will be reimplemented later         *
            **************************************/
            /*int newVertices = MathUtils.random(3,8);
            if(verticesNum != newVertices) {
                verticesNum = newVertices;
                rotateVelocity = 2 * MathUtils.PI / (verticesNum * (-2 * jumpVelocity / acceleration.y));
                realCenter = bottom + (radius * MathUtils.cos((MathUtils.PI / verticesNum)));
                vertices = new Vector2[verticesNum];
                createVertices();
            } else return 10;*/
            numProjectiles+=4;
            if(numProjectiles > MAXPROJECTILES) numProjectiles = MAXPROJECTILES;

        }else points = 10;

        world.addScore(points);

        return points;
    }

    public ArrayList<Projectile> getProjectiles(){
        return projectiles;
    }


    public float getBottom(){
        return bottom;
    }

    public float[] getVertices(){
        float[] vertices = new float[verticesNum *2];
        int i;
        for(i = 0 ; i < verticesNum *2; i++){
            vertices[i]=( i%2==0 ? this.vertices[i/2].x : this.vertices[i/2].y );
            //Gdx.app.log(i+"" ,": " +vertices[i]);
        }

        return vertices;
    }

    public Rectangle getBound(){
        return bound;
    }

    public void setJumping(boolean b){
        jumping = b;
    }

    public void setPosition(float y){
        position.y=y;
        theta = ((verticesNum+1)%2)*MathUtils.PI/verticesNum;
        computeVertices();
    }

    public void adjustSpeed(float ratio){
        jumpVelocity *=ratio;
        acceleration.y *= (ratio*ratio);
        rotateVelocity *= ratio;
    }

    public void loseLife(){
        if(!invincible){
            color = IMMORTAL;
            lives--;
            invincible = true;
            invincibleTime = 0;
        }
        if(lives <= 0) {
            alive = false;
            color = new Color();
        }

    }

    public int getLives(){
        return lives;
    }

    public boolean isJumping() {
        return jumping;
    }
    public boolean isAlive() {
        return alive;
    }

    public float getRealCenter() {
        return realCenter;
    }

    public int getNumProjectiles() {
        return numProjectiles;
    }

    public void setJump(boolean jump){
        this.jump = jump;
    }

    public void setShoot(boolean shoot){
        this.shoot = shoot;
    }

    public void switchSide(){
        if(jump){
            jump = false;
            shoot = true;
        }
        if(shoot){
            shoot = true;
            jump = false;
        }
    }

    public Vector2 getPosition(){
        return position;
    }

    public float getRadius(){
        return radius;
    }

    @Override
    public void draw(GameRenderer renderer) {
        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        renderer.getShapeRenderer().setColor(color);

        float[] vertices = getVertices();

        int i;
        for ( i = 0 ; i < 2*verticesNum; i+=2){
            vertices[i] = renderer.getOrientation().getOx() + renderer.getOrientation().getDx() *vertices[i];
            vertices[i+1] = renderer.getOrientation().getOy() + renderer.getOrientation().getDy() *vertices[i+1];
        }
        for ( i = 0 ; i < 2*verticesNum; i+=2){
            renderer.getShapeRenderer().rectLine(vertices[i], vertices[i + 1], vertices[(i + 2) % (2 * verticesNum)], vertices[(i + 3) % (2 * verticesNum)], 1);
        }

        renderer.getShapeRenderer().end();


        PolygonSprite polySprite = new PolygonSprite(new PolygonRegion(TEXREG, vertices , TRIANGULATOR.computeTriangles(vertices).toArray()));
        renderer.getPolySBatch().begin();
        polySprite.setScale(renderer.getScaleX(), renderer.getScaleY());
        polySprite.draw(renderer.getPolySBatch());
        renderer.getPolySBatch().end();
    }
}
