package it.fmatticchio.gameobjects;
import com.badlogic.gdx.math.MathUtils;
import it.fmatticchio.screen.GameScreen;

/**
 * Created by francesco on 19/01/16.
 */
public class Star {
    private float x;



    private float y;
    private float velx;

    public Star(float x, float y, float velocity){
        this.x = x;
        this.y = y;
        this.velx = velocity;
    }

    public void update(float delta){
        x+=velx*delta;
        if ( x < 0 || y < 0 || y > GameScreen.SCREENY){
            x = GameScreen.SCREENX;
            y = MathUtils.random(70, GameScreen.SCREENY) ;
        }
    }

    public float getY() {
        return y;
    }

    public float getX() {
        return x;
    }
}
