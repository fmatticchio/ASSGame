package it.fmatticchio.gameobjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by francesco on 19/01/16.
 */
public abstract class Scrollable implements Drawable {


    protected Vector2 position;
	protected Vector2 velocity;
	protected float width;
	protected float height;
	protected boolean isDead;
    private Shape2D bound;

	protected Scrollable(float x, float y, float width, float height, float scrollSpeed) {
		position = new Vector2(x, y);
		velocity = new Vector2(scrollSpeed, 0);
		this.width = width;
		this.height = height;
        isDead=false;
    }

	public void update(float delta) {
		position.add(velocity.cpy().scl(delta));

		// If the Scrollable object is no longer visible:
		if (position.x + width < 0) {
			isDead = true;
		}
	}

	// Reset: Should Override in subclass for more specific behavior.
	public void reset(float newX) {
		position.x = newX;
		isDead = false;
	}

	public void stop() {
		velocity.x = 0;
	}

    public void die(){
        isDead = true;
    }

    public abstract boolean collides(Projectile p);

    public abstract boolean collides(Player p);
	// Getters for instance variables
	public boolean isDead() {
		return isDead;
	}

	public float getTailX() {
		return position.x + width;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

    public Shape2D getBound(){
        return bound;
    }


}
