package it.fmatticchio.gameobjects;

import com.badlogic.gdx.math.MathUtils;

/**
 * Creates the scrollable elements
 */
public class ScrollableFactory {
    public static Scrollable createScrollable(float x, float y, float scrollSpeed){
        int enemyType = MathUtils.random(0,13);
        Scrollable s;
        switch (enemyType){
            case 3: case 1:case 2:case 10:case 13:
                if(MathUtils.random(1f)<.95) {
                    s = new Enemy(x, y + 15, 15, scrollSpeed);
                }
                else s = new Enemy(x , y+ 30 , 30 , scrollSpeed);
                break;
            case 6: case 4: case 5:case 11:
                s = new Spike(x, y, 30, 30, scrollSpeed);
                break;
            case 9: case 7: case 8:case 12:
                s= new Block(x, y, 30* MathUtils.random(1,3.2f), 30* MathUtils.random(.9f, 1.5f), scrollSpeed);
                break;
            case 0:
                s= new Coin(x, y, 30, 30, scrollSpeed);
                break;
            default:
                s = new Enemy(x,y+15,15,scrollSpeed);
                break;

        }
        return s;
    }
}
