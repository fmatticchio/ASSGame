package it.fmatticchio.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;

import it.fmatticchio.gameworld.GameRenderer;
import it.fmatticchio.gameworld.GameWorld;
import it.fmatticchio.helpers.AssetsLoader;

/**
 * Enemy class.
 */
public class Enemy extends Scrollable {

    private Circle bound;

    public Enemy(float x, float y, float radius, float scrollSpeed) {
        super(x-radius, y-radius, radius*2 , radius*2 , scrollSpeed);

        bound = new Circle(x,y,radius);
    }

    @Override
    public void update(float delta){
        super.update(delta);
        bound.x+= delta*velocity.x;
    }

    @Override
    public boolean collides (Projectile p){
        boolean r =  bound.contains(p.getPosition());
        if(r){
            AssetsLoader.hit.play(AssetsLoader.volume());
            die();
            p.destroy();

            if(isDead()) {
                GameWorld.getWorld().addScore(5);
                Coin c = new Coin(getX(), getY(), getWidth(), getHeight(), GameWorld.getWorld().getScrollSpeed());
                GameWorld.getWorld().getScrollables().add(GameWorld.getWorld().getScrollables().indexOf(this), c);
            } else GameWorld.getWorld().addScore(45);
        }
        return r;
    }

    @Override
    public boolean collides(Player p){
        boolean r = Intersector.overlaps(bound, p.getBound());
        if(r) {
            AssetsLoader.collision.play(AssetsLoader.volume());
            p.loseLife();
            die();
            die();
        }
        return r;
    }

    @Override
    public void die(){
        if(bound.radius <= 20f) {
            super.die();
            return;
        }
        height -= 30;
        width -= 30;
        bound.radius -= 15;
        bound.setPosition(position.x+bound.radius , position.y + bound.radius );

    }

    @Override
    public Circle getBound(){
        return bound;
    }


    @Override
    public void draw(GameRenderer renderer) {
        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Line);
        renderer.getShapeRenderer().setColor(Color.RED);

        renderer.getShapeRenderer().circle(renderer.getOrientation().getOx() + renderer.getOrientation().getDx() * getBound().x, renderer.getOrientation().getOy() + renderer.getOrientation().getDy() * getBound().y, getBound().radius);
        renderer.getShapeRenderer().end();

        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        renderer.getShapeRenderer().setColor(Color.PURPLE);

        renderer.getShapeRenderer().circle(renderer.getOrientation().getOx() + renderer.getOrientation().getDx() * getBound().x, renderer.getOrientation().getOy() + renderer.getOrientation().getDy() * getBound().y, getBound().radius);
        renderer.getShapeRenderer().end();

    }
}
