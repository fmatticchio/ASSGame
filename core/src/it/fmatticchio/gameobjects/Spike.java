package it.fmatticchio.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import it.fmatticchio.gameworld.GameRenderer;
import it.fmatticchio.helpers.AssetsLoader;

/**
 * Created by francesco on 19/01/16.
 */
public class Spike extends Scrollable {
    private Rectangle bound;

    private static Pixmap PIX = new Pixmap(1,1, Pixmap.Format.RGBA4444);
    static {
        PIX.setColor(Color.LIGHT_GRAY);
        PIX.fill();
    }
    private static Texture TEX = new Texture(PIX);
    private static TextureRegion TEXREG = new TextureRegion(TEX);
    private static EarClippingTriangulator TRIANGULATOR = new EarClippingTriangulator();
    public static void dispose(){
        PIX.dispose();
        TEX.dispose();
    }

    public Spike(float x, float y, float width, float height, float scrollSpeed) {
        super(x, y, width, height, scrollSpeed);
        bound = new Rectangle(x,y,width,height);
    }

    @Override
    public void update(float delta){
        super.update(delta);
        bound.x+= delta*velocity.x;

    }

    @Override
    public boolean collides(Projectile p){
        boolean r =  bound.contains(p.getPosition());
        if(r){
            AssetsLoader.hitWall.play(AssetsLoader.volume());
            p.destroy();
        }
        return r;
    }

    @Override
    public boolean collides(Player p){
        boolean r = Intersector.overlaps(bound, p.getBound());
        if(r) {
            AssetsLoader.collision.play(AssetsLoader.volume());
            p.loseLife();
            if(!p.isJumping()){
                die();
            }
            p.setJumping(false);
            p.jump();
        }
        return r;
    }

    @Override
    public Rectangle getBound(){
        return bound;
    }

    @Override
    public void draw(GameRenderer renderer) {

        float[] vertices = new float[14];
        float step = getWidth()/6;
        int j;
        for(j = 0 ; j < 14 ; j++){
            switch(j%4){
                case 0:case 2:
                    vertices[j]= renderer.getOrientation().getOx() + renderer.getOrientation().getDx() *(getX()+(j*step/2));
                    break;
                case 1:
                    vertices[j]= renderer.getOrientation().getOy() + renderer.getOrientation().getDy() *getY();
                    break;
                case 3:
                    vertices[j]= renderer.getOrientation().getOy() + renderer.getOrientation().getDy() *(getY()+getHeight());
                    break;
            }
        }
        PolygonSprite polySprite = new PolygonSprite(new PolygonRegion(TEXREG, vertices , TRIANGULATOR.computeTriangles(vertices).toArray()));
        renderer.getPolySBatch().begin();
        polySprite.setScale(renderer.getScaleX(), renderer.getScaleY());
        polySprite.draw(renderer.getPolySBatch());
        renderer.getPolySBatch().end();

        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Line);
        renderer.getShapeRenderer().setColor(Color.RED);
        renderer.getShapeRenderer().polygon(vertices);
        renderer.getShapeRenderer().end();
    }
}
