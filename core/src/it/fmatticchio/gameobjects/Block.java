package it.fmatticchio.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import it.fmatticchio.gameworld.GameRenderer;
import it.fmatticchio.helpers.AssetsLoader;

/**
 * Created by francesco on 20/01/16.
 */
public class Block extends Scrollable {

    private Rectangle bound;

    public Block(float x, float y, float width, float height, float scrollSpeed) {
        super(x, y, width, height, scrollSpeed);
        bound = new Rectangle(x, y, width, height);
    }

    @Override
    public void update(float delta){
        super.update(delta);
        bound.x+= delta*velocity.x;

    }

    @Override
    public boolean collides(Projectile p){
        boolean r =  bound.contains(p.getPosition());
        if(r){
            AssetsLoader.hitWall.play(AssetsLoader.volume());
            p.destroy();
        }
        return r;
    }

    @Override
    public boolean collides(Player p){
        boolean r = Intersector.overlaps(bound, p.getBound());
        if(r) {
            AssetsLoader.collision.play(AssetsLoader.volume());
            if(p.collidesSide(this)) {
                AssetsLoader.collision.play(AssetsLoader.volume());
                die();
                p.loseLife();
            } else if(p.collidesTop(this) ) {
                p.setJumping(false);
                p.setPosition(getHeight()+p.getRealCenter());
            }
        }//TODO
        return r;
    }

    @Override
    public Rectangle getBound(){
        return bound;
    }

    @Override
    public void draw(GameRenderer renderer) {
        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Line);
        renderer.getShapeRenderer().setColor(Color.RED);
        renderer.getShapeRenderer().rect(renderer.getOrientation().getOx() + renderer.getOrientation().getDx() * getX(), renderer.getOrientation().getOy() + renderer.getOrientation().getDy() * getY(), renderer.getOrientation().getDx() * getWidth(), renderer.getOrientation().getDy() * getHeight());
        renderer.getShapeRenderer().end();

        renderer.getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        renderer.getShapeRenderer().setColor(Color.GOLDENROD);
        renderer.getShapeRenderer().rect(renderer.getOrientation().getOx() + renderer.getOrientation().getDx() * getX(), renderer.getOrientation().getOy() + renderer.getOrientation().getDy() * getY(), renderer.getOrientation().getDx() * getWidth(), renderer.getOrientation().getDy() * getHeight());
        renderer.getShapeRenderer().end();
    }
}
