package it.fmatticchio.gameobjects;



import it.fmatticchio.gameworld.GameRenderer;

/**
 * Created by francesco on 04/04/16.
 */
public interface Drawable {
    void draw(GameRenderer renderer);

}
