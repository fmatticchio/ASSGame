package it.fmatticchio.gameworld;

import com.badlogic.gdx.math.MathUtils;
import it.fmatticchio.gameobjects.Player;
import it.fmatticchio.gameobjects.ScrollableFactory;
import it.fmatticchio.gameobjects.Star;
import it.fmatticchio.gameobjects.Scrollable;
import it.fmatticchio.game.ASSGame;
import it.fmatticchio.gameobjects.Coin;
import it.fmatticchio.gameobjects.Projectile;
import it.fmatticchio.helpers.AchievementsHandler;
import it.fmatticchio.helpers.AssetsLoader;
import it.fmatticchio.screen.GameScreen;

import java.util.ArrayList;


/**
 * Where the real game happens
 */
public class GameWorld {
    public static final float BASELINE = 50f , SCROLLSPEEDINIT = -250;

    private static GameWorld world = new GameWorld();

    public enum GameState{
        RUNNING, START, GAMEOVER, PAUSE
    }

    private int dialogMessage = 1000000; //some big number

    public class Floor{
        public float position, length, velocity;

        public Floor(float position, float length, float velocity ){
            this.position = position;
            this.length = length;
            this.velocity = velocity;
        }

        public boolean update(float delta){
            position += velocity*delta;
            return position+length < 0;
        }
    }

    private boolean highScore;
    private boolean dialogOn;

    private GameState currentState;

    private GameRenderer renderer;
    private ASSGame game;

    private Player player;
    private ArrayList<Scrollable> scrollables;
    public Star[] sky;
    public int starNum;
    public Floor floor1, floor2;

    private float scrollerWait, nextScroller, scrollSpeed, secs, speedRatio;

    private int score;

    private GameWorld(){
        currentState = GameState.START;
        dialogOn = false;

        player = new Player( 50 , 100 , 20 , this);
        scrollables = new ArrayList<Scrollable>();

        scrollerWait = 0;
        populateSky();
        nextScroller = 250f;
        score = 0;
        highScore = false;

        scrollSpeed = SCROLLSPEEDINIT;
        speedRatio = scrollSpeed/SCROLLSPEEDINIT;

        floor1 = new Floor( 0 , GameScreen.SCREENX , scrollSpeed);
        floor2 = new Floor( GameScreen.SCREENX , GameScreen.SCREENX , scrollSpeed);

        secs = 0;
    }

    public static GameWorld getWorld(){
        return world;
    }

    public void setGame(ASSGame game){
        this.game = game;
    }

    public void reset() {
        player.restart(50, 100, 20);
        scrollables.clear();
        score = 0;
        highScore = false;
        nextScroller = 250f;

        scrollSpeed = SCROLLSPEEDINIT;
        speedRatio = scrollSpeed/SCROLLSPEEDINIT;
        scrollerWait = 0;
        secs = 0;

        renderer.setOrientation(GameRenderer.Orientation.NORMAL);
    }
    public void start(){
        currentState = GameState.RUNNING;
        AssetsLoader.playButton.click();
    }

    private void populateSky(){
        int i;
        starNum = MathUtils.random(12,18);
        sky = new Star[starNum];
        for (i = 0 ; i < starNum ; i++){
            sky[i] = new Star(MathUtils.random(0, GameScreen.SCREENX),MathUtils.random(70, GameScreen.SCREENY) , MathUtils.random(-20 , -10));
        }
    }

    public void update(float delta){
        if(!dialogOn) {
            switch (currentState) {
                case START:
                    int i;
                    for (i = 0; i < starNum; i++) {
                        sky[i].update(delta);
                    }
                    // updateFloor(delta);
                    player.update(delta);
                    break;
                case GAMEOVER:
                case PAUSE:
                    break;
                case RUNNING:
                    updateFloor(delta);
                    updateRunning(delta);
                    break;
            }
        }
    }

    private void updateFloor(float delta){
        boolean f1,f2;
        f1 = floor1.update(delta);
        f2 = floor2.update(delta);

        if(f1) floor1.position = (floor2.position+floor2.length);
        if(f2) floor2.position = (floor1.position+floor1.length);
    }

    private void updateRunning(float delta) {
        scrollerWait+=delta;

        if(-1*scrollerWait*scrollSpeed>nextScroller){
            scrollables.add(ScrollableFactory.createScrollable(GameScreen.SCREENX + 50, player.getBottom(), scrollSpeed));

            scrollerWait = 0;
            nextScroller  = MathUtils.random(150f, 300f);
        }
        player.update(delta);

        int i,j;
        for (i = 0 ; i < starNum ; i++){
            sky[i].update(delta);
        }

        for (i = 0; i < player.getProjectiles().size(); i++) {
            Projectile p = player.getProjectiles().get(i);
            p.update(delta);
            if (!p.isVisible()) player.getProjectiles().remove(i);
        }
        for(j = 0 ; j < scrollables.size() ; j++) {
            scrollables.get(j).update(delta);
             if (!scrollables.get(j).isDead()){
                 scrollables.get(j).collides(player);
                 for (i = 0; i < player.getProjectiles().size(); i++) {
                    scrollables.get(j).collides(player.getProjectiles().get(i));
                 }

             }
            if (scrollables.get(j).isDead()) scrollables.remove(j);
        }

        secs+=delta;
        if (player.isAlive() && secs > 2 ){
            secs -= 2;
            addScore(1);
        }

    }

    public void setRenderer(GameRenderer renderer) {
        this.renderer = renderer;
    }

    public void addScore(int points){
        if(score <= 600 && score % 25 +points >= 25 ){
            speedRatio = (scrollSpeed-10)/scrollSpeed;
            scrollSpeed *=speedRatio;
            player.adjustSpeed(speedRatio);
            floor1.velocity = scrollSpeed;
            floor2.velocity = scrollSpeed;
        }
        score+=points;
    }

    public Player getPlayer() {
        return player;
    }

    public ArrayList<Scrollable> getScrollables() {
        return scrollables;
    }

    public int getScore() {
        return score;
    }

    public GameRenderer getRenderer() {
        return renderer;
    }

    public GameState getCurrentState() {
        return currentState;
    }

    public void gameOver(){
        currentState = GameState.GAMEOVER;
        AssetsLoader.playButton.click();
        if(highScore = score > AssetsLoader.getHighScore()){
            AssetsLoader.setHighScore(score);
        }
        if(game.getServices().isSignedIn()) game.getServices().submitScore(score);
        AchievementsHandler.onGameOver(score);
    }

    public void pause(){
        currentState = GameState.PAUSE;
        AssetsLoader.playButton.click();
    }

    public void openDialog(int d){
        dialogOn = true;
        dialogMessage = d;
    }

    public void resume(){
        dialogOn = false;
        dialogMessage = 100000; //just a really big number.
    }


    public boolean isHighScore(){
        return highScore;
    }

    public ASSGame getGame() {
        return game;
    }

    public float getScrollSpeed() {
        return scrollSpeed;
    }

    public boolean isDialogOn() {
        return dialogOn;
    }

    public void setDialogOn(boolean dialogOn) {
        this.dialogOn = dialogOn;
    }

    public int getDialog() {
        return dialogMessage;
    }

  /*  public void setDialog(int dialogMessage) {
        this.dialogMessage = dialogMessage;
    }*/
}
