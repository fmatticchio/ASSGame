package it.fmatticchio.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;

import it.fmatticchio.game.StringsHandler;
import it.fmatticchio.gameobjects.Player;
import it.fmatticchio.gameobjects.Projectile;
import it.fmatticchio.gameobjects.Spike;
import it.fmatticchio.gameobjects.Scrollable;
import it.fmatticchio.helpers.AchievementsHandler;
import it.fmatticchio.helpers.AssetsLoader;
import it.fmatticchio.screen.GameScreen;
import it.fmatticchio.ui.GPGDialog;
import it.fmatticchio.ui.Options;
import it.fmatticchio.ui.ShareDialog;
import it.fmatticchio.ui.YNDialog;

import java.util.ArrayList;

/**
 * Renderer class
 */
public class GameRenderer {
    public enum Orientation  {
        NORMAL(0,0,1,1), UPSIDEDOWN(0,GameScreen.SCREENY , 1 , -1), MIRROR(GameScreen.SCREENX , 0, -1 , 1), R180(GameScreen.SCREENX , GameScreen.SCREENY , -1 , -1);

        private int ox, oy, dx, dy;
 
        Orientation(int ox, int oy, int dx, int dy) {
            this.ox = ox;
            this.oy = oy;
            this.dx = dx;
            this.dy = dy;
        }

        public int getOx() {
            return ox;
        }

        public int getOy() {
            return oy;
        }

        public int getDx() {
            return dx;
        }

        public int getDy() {
            return dy;
        }

        private static final Orientation[] DIRECTIONS = values();
        public static Orientation random(){
            return DIRECTIONS[(MathUtils.random(3))];
        }
    }

    private Orientation orientation;

    // Environment classes
    private GameWorld world;
    private OrthographicCamera cam;
    private SpriteBatch batcher;
    private ShapeRenderer shapeRenderer;

    private float scaleX, scaleY;

    public float getScaleX() {
        return scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    private PolygonSpriteBatch polySBatch;
    private BitmapFont font;


    //Elements classes
    private Player player;
    private ArrayList<Scrollable> scrollables;

    public static final Rectangle SHAREICON = new Rectangle(GameScreen.SCREENX/2f + 20 , 50 , 20 , 20); //very stupid way to do a share icon


    public PolygonSpriteBatch getPolySBatch() {
        return polySBatch;
    }

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public SpriteBatch getBatcher() {
        return batcher;
    }

    public GameRenderer(GameWorld world , float scaleX, float scaleY){

        this.scaleX = scaleX;
        this.scaleY = scaleY;

        this.world = world;
        this.player = world.getPlayer();
        this.scrollables = world.getScrollables();

        orientation = Orientation.NORMAL;

        cam = new OrthographicCamera();
        cam.setToOrtho(false, GameScreen.SCREENX, GameScreen.SCREENY);

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(cam.combined);

        polySBatch = new PolygonSpriteBatch();


        font = new BitmapFont();

        batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);


    }

    public void dispose(){
        shapeRenderer.dispose();
        font.dispose();
        batcher.dispose();
        polySBatch.dispose();
        Spike.dispose();
        Player.dispose();
    }

    public void render(float delta, float runTime){

        //Black background to avoid flickering
        Gdx.gl.glClearColor(0, 0, 100 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderSky();
        renderGround();

        player.draw(this);
        renderScrollables();
        renderProjectiles();

        renderText(delta, runTime);

        if(world.isDialogOn()){
            renderDialog(world.getDialog());
        }

//        renderTest();

    }

    private void renderGround(){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.FIREBRICK);
        shapeRenderer.rect(orientation.ox, orientation.oy, orientation.dx * GameScreen.SCREENX, orientation.dy * player.getBottom());
        shapeRenderer.end();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BROWN);
        shapeRenderer.line(orientation.ox, orientation.oy + orientation.dy * player.getBottom(), GameScreen.SCREENX, orientation.oy + orientation.dy * player.getBottom());
        shapeRenderer.end();

        batcher.begin();
        batcher.draw(AssetsLoader.floor1, orientation.ox + orientation.dx * world.floor1.position, orientation.oy, orientation.dx * world.floor1.length, orientation.dy * player.getBottom());
        batcher.draw(AssetsLoader.floor2 , orientation.ox + orientation.dx *world.floor2.position , orientation.oy, orientation.dx *world.floor2.length , orientation.dy * player.getBottom());
        batcher.end();

    }



 /*   private void renderTest(){
        int i;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.GREEN);
        for(i = 0 ; i < GameScreen.SCREENX ; i+=5){
            shapeRenderer.line(i , 0 , i , GameScreen.SCREENY);
        }
        for(i = 0 ; i < GameScreen.SCREENY ; i+= 5){
            shapeRenderer.line(0 , i , GameScreen.SCREENX ,i);
        }
        shapeRenderer.end();
    }*/

    private void renderProjectiles(){

        int i;
        for(i = 0; i < player.getProjectiles().size() ; i++){
            player.getProjectiles().get(i).draw(this);
        }
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.YELLOW);
        for (i = 0 ; i < player.getNumProjectiles() ; i++){
            shapeRenderer.circle( GameScreen.SCREENX - 10 - 7*(10-i%10), 20 - 10*(i/10) , 3f);
        }
        shapeRenderer.end();
    }

    private void renderScrollables(){
        int i ;

        for(i = 0 ; i < scrollables.size() ; i++){
            if(!this.scrollables.get(i).isDead()) scrollables.get(i).draw(this);
        }
    }

    private void renderText(float delta, float runTime){

        batcher.begin();

        font.draw(batcher, "" + world.getScore(), 20, GameScreen.SCREENY - 20);

        int i;
        for (i = 0 ; i < player.getLives() ; i++){
            AssetsLoader.heart.draw(batcher , 10 + 25 * i, 10);
        }

        batcher.end();

        switch (world.getCurrentState()) {
            case GAMEOVER:
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(0.05f, 0.05f, 0.051f, .2f);
                shapeRenderer.rect(0,0, GameScreen.SCREENX , GameScreen.SCREENY);
                shapeRenderer.end();
                batcher.begin();
                if (world.isHighScore()) {
                    font.draw(batcher, StringsHandler.getString(StringsHandler.NEWHS)+"\n\n" + world.getScore(), GameScreen.SCREENX / 3f, (GameScreen.SCREENY + GameWorld.BASELINE) / 2f, GameScreen.SCREENX / 3f, Align.center, false);
                } else {
                    font.draw(batcher, StringsHandler.getString(StringsHandler.GAMEOVER)+"\n\n" + world.getScore() + "\n\n"+ StringsHandler.getString(StringsHandler.HIGHSCORE) + " " + AssetsLoader.getHighScore(), GameScreen.SCREENX / 3f, (GameScreen.SCREENY + GameWorld.BASELINE) * 0.7f, GameScreen.SCREENX / 3f, Align.center, false);
                }
                batcher.end();
                break;
            case START:
                batcher.begin();
                font.draw(batcher, StringsHandler.getString(StringsHandler.START), GameScreen.SCREENX / 3f, (GameScreen.SCREENY + GameWorld.BASELINE) / 2f, GameScreen.SCREENX / 3f, Align.center, true);
                batcher.end();
                break;
            case PAUSE:
                batcher.begin();
                font.draw(batcher, StringsHandler.getString(StringsHandler.PAUSE), GameScreen.SCREENX / 3f, (GameScreen.SCREENY + GameWorld.BASELINE) / 2f, GameScreen.SCREENX / 3f, Align.center, true);
                batcher.end();
                break;
        }

        batcher.begin();

       // AssetsLoader.volumeButton.draw(batcher);
        AssetsLoader.playButton.draw(batcher);
        if(world.getCurrentState() == GameWorld.GameState.RUNNING) AssetsLoader.reset.draw(batcher);
        batcher.end();

        if(world.getCurrentState()== GameWorld.GameState.START || world.getCurrentState()== GameWorld.GameState.GAMEOVER || world.getCurrentState()== GameWorld.GameState.PAUSE){
            batcher.begin();
            AssetsLoader.gPlayLogIn.draw(batcher);
            AssetsLoader.volume.draw(batcher);
            batcher.end();
            drawShareIcon();
           // AssetsLoader.gPlayLeaderboards.draw(batcher);
           // AssetsLoader.gPlayAchievement.draw(batcher);
        }


        /*********************************
        **TEST CODE SHOWING TAP LOCATION**
        *********************************/
        /*
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.circle(AssetsLoader.x, GameScreen.SCREENY - AssetsLoader.y, 10);
        shapeRenderer.end();*/

    }

    private void drawShareIcon(){

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.circle(SHAREICON.x + 5, SHAREICON.y + 10, 3);
        shapeRenderer.circle(SHAREICON.x + 15, SHAREICON.y + 5, 3);
        shapeRenderer.circle(SHAREICON.x + 15, SHAREICON.y + 15, 3);
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.line(SHAREICON.x + 5, SHAREICON.y + 10, SHAREICON.x + 15, SHAREICON.y + 5);
        shapeRenderer.line(SHAREICON.x + 5, SHAREICON.y + 10 , SHAREICON.x + 15, SHAREICON.y + 15);
        shapeRenderer.end();
    }

    private void renderSky(){
        int i;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);
        for (i = 0 ; i < world.starNum ; i++){
            shapeRenderer.circle(orientation.ox + orientation.dx *world.sky[i].getX() , orientation.oy + orientation.dy *world.sky[i].getY() , 2f);
        }
        shapeRenderer.end();
    }

    private void renderDialog(int i){
        switch(i){
            case StringsHandler.QUIT: case StringsHandler.SIGNOUT: case StringsHandler.SIGNIN: case StringsHandler.RESTART:
                renderYNDialog(StringsHandler.getString(i));
                break;
            case StringsHandler.GPGDIALOG:
                renderGPGDialog();
                break;
            case StringsHandler.OPTIONS:
                renderOptions();
                break;
            case StringsHandler.SHARE:
                renderShare();
                break;
        }
    }

    private void renderYNDialog(String s){
        YNDialog.draw(shapeRenderer , batcher , s ,font);
    }
    private void renderGPGDialog(){
        GPGDialog.draw(shapeRenderer , batcher ,font , world.getGame().getServices().isSignedIn());
    }

    private void renderOptions(){
        Options.draw(shapeRenderer);
    }

    private void renderShare(){
        ShareDialog.draw(shapeRenderer, batcher, font);
    }

    public void setOrientation(Orientation o){
        if(o.dx != orientation.dx) player.switchSide();
        orientation = o;
        AchievementsHandler.addOrientation(orientation);

    }

    public Orientation getOrientation() {
        return orientation;
    }
}
