package it.fmatticchio.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;

import it.fmatticchio.game.StringsHandler;
import it.fmatticchio.screen.GameScreen;

/**
 * Abstract class that handles the YES/NO dialogs
 */
public class YNDialog {
    public enum Result{
        YES, NO , OUT , NULL
    }

    private static Rectangle
            DIALOG = new Rectangle(GameScreen.SCREENX/4f , GameScreen.SCREENY/3f , GameScreen.SCREENX/2f , GameScreen.SCREENY/3f ) ,
            YES = new Rectangle(DIALOG.x + 10 , DIALOG.y +10 , DIALOG.width / 2 -15 , 20),
            NO = new Rectangle(DIALOG.x + DIALOG.width/2 + 5 , DIALOG.y +10 , DIALOG.width / 2 -15 , 20);

    public static void draw(ShapeRenderer render , Batch batcher , String text , BitmapFont font){
        render.begin(ShapeRenderer.ShapeType.Filled);
        render.setColor(Color.BLACK);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);
        render.setColor(Color.DARK_GRAY);
        render.rect(YES.x , YES.y , YES.width, YES.height);
        render.rect(NO.x, NO.y, NO.width, NO.height);
        render.end();

        render.begin(ShapeRenderer.ShapeType.Line);
        render.setColor(Color.RED);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);
        render.setColor(Color.WHITE);
        render.rect(YES.x, YES.y, YES.width, YES.height);
        render.rect(NO.x, NO.y, NO.width, NO.height);
        render.end();

        batcher.begin();
        font.draw(batcher, text, DIALOG.x + 20, DIALOG.y+DIALOG.height - 15, DIALOG.width - 40, Align.left, true);

        font.draw(batcher, StringsHandler.getString(StringsHandler.YES), YES.x+YES.width/2 -15, YES.y + 15);
        font.draw(batcher, StringsHandler.getString(StringsHandler.NO), NO.x+NO.width/2 -15, NO.y + 15);
        batcher.end();

    }

    public static Result click(float x , float y){
        if(!DIALOG.contains(x,y)) return Result.OUT;
        if(YES.contains(x,y)) return Result.YES;
        if(NO.contains(x,y)) return Result.NO;

        return Result.NULL;
    }

}
