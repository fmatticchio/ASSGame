package it.fmatticchio.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import it.fmatticchio.screen.GameScreen;

/**
 * Icon class
 */
public class CustomIcon {
    private float x, y, width, height;
    private TextureRegion icon;

    private Rectangle bounds;

    public CustomIcon(float x, float y, float width, float height, TextureRegion icon) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.icon = icon;
        bounds = new Rectangle(x,y,width,height);
    }

    public boolean isClicked(float x, float y){
        y = GameScreen.SCREENY - y;
        return bounds.contains(x,y);
    }

    public void draw(SpriteBatch batcher){
        batcher.draw(icon, x, y, width, height);
    }

    /*NEVER CALL THIS METHOD IF YOU CAN CLICK ON THE ICON!!!!*/
    public void draw(SpriteBatch batcher , float x ,  float y){
        batcher.draw(icon, x, y , width , height);
    }
}
