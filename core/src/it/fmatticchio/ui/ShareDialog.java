package it.fmatticchio.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;

import it.fmatticchio.game.StringsHandler;
import it.fmatticchio.gameworld.GameWorld;
import it.fmatticchio.screen.GameScreen;

/**
 * Share Dialog
 */
public class ShareDialog {

    public enum Result{
        SHARE, RATE, SCORE ,  OUT , NULL
    }

    private static Rectangle
            DIALOG = new Rectangle(GameScreen.SCREENX/4f , GameScreen.SCREENY/5f , GameScreen.SCREENX/2f , 3*GameScreen.SCREENY/5f ) ,
            RATE = new Rectangle(DIALOG.x + 10 , DIALOG.y +10 , DIALOG.width - 20 , 20),
            SHARE = new Rectangle(DIALOG.x + 10 , DIALOG.y +35 , DIALOG.width - 20 , 20),
            SCORE = new Rectangle(DIALOG.x + 10 , DIALOG.y +60 , DIALOG.width - 20 , 20);

    public static void draw(ShapeRenderer render , Batch batcher ,  BitmapFont font ){

        render.begin(ShapeRenderer.ShapeType.Filled);
        render.setColor(Color.BLACK);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);
        render.setColor(Color.DARK_GRAY);
        render.rect(SHARE.x, SHARE.y, SHARE.width, SHARE.height);
        render.rect(RATE.x, RATE.y, RATE.width, RATE.height);
        render.rect(SCORE.x, SCORE.y, SCORE.width, SCORE.height);
        render.end();

        render.begin(ShapeRenderer.ShapeType.Line);
        render.setColor(Color.RED);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);
        render.setColor(Color.WHITE);
        render.rect(SHARE.x, SHARE.y, SHARE.width, SHARE.height);
        render.rect(RATE.x, RATE.y, RATE.width, RATE.height);
        render.rect(SCORE.x, SCORE.y, SCORE.width, SCORE.height);
        render.end();

        batcher.begin();
        font.draw(batcher, "Do you like this game? Share it with your friends and rate it!", DIALOG.x + 20, DIALOG.y+DIALOG.height - 15, DIALOG.width - 40, Align.left, true);

        //TODO handle the strings
        font.draw(batcher, "Share the game", SHARE.x+15, SHARE.y + 15);
        font.draw(batcher, "Rate the game", RATE.x+15, RATE.y + 15);
        font.setColor((GameWorld.getWorld().getCurrentState() == GameWorld.GameState.GAMEOVER ? Color.WHITE : Color.GRAY));
        font.draw(batcher, "Share your score", SCORE.x+15, SCORE.y + 15);
        font.setColor(Color.WHITE);
        batcher.end();

    }

    public static Result click(float x , float y){
        if(!DIALOG.contains(x,y)) return Result.OUT;
        if(SHARE.contains(x,y)) return Result.SHARE;
        if(RATE.contains(x,y)) return Result.RATE;
        if(SCORE.contains(x,y)) return Result.SCORE;
        return Result.NULL;
    }
}
