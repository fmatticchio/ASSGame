package it.fmatticchio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import it.fmatticchio.screen.GameScreen;

/**
 * ON/OFF Button class
 */
public class CheckButton {
    private float x, y, width, height;
    private TextureRegion on, off;

    private Rectangle bounds;
    private boolean isOn;

    public CheckButton(float x, float y, float width , float height , TextureRegion on , TextureRegion off, boolean isOn){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.on = on;
        this.off = off;
        this.isOn = isOn;
        bounds = new Rectangle(x,y,width,height);
    }
    public CheckButton(float x, float y, float width , float height , TextureRegion on , TextureRegion off){
        this(x,y,width,height,on,off, true);
    }

    public boolean isClicked(float x, float y){
        y = GameScreen.SCREENY - y;

        return bounds.contains(x,y);

    }

    public void click(){
        isOn = !isOn;
    }

    public boolean isOn(){
        return isOn;
    }

    public void draw(SpriteBatch batcher){
        batcher.draw((isOn ? on : off), x , y , width , height);
    }

    public Rectangle getBounds(){
        return bounds;
    }


}
