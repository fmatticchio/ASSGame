package it.fmatticchio.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;

import it.fmatticchio.helpers.AssetsLoader;
import it.fmatticchio.screen.GameScreen;

/**
 * Option Dialog
 */
public class Options {

    public enum Result{
        SLIDER , VOLUME , OUT , NULL
    }

    private static boolean dragging = false;

    private static Rectangle
            DIALOG = new Rectangle(GameScreen.SCREENX/4f , GameScreen.SCREENY/2f - 25, GameScreen.SCREENX/2f , 50 ) , VOLUME = new Rectangle(DIALOG.x + 10 , DIALOG.y + 20 , DIALOG.width-20 , 10);
    private static Circle SLIDER = new Circle(VOLUME.x + VOLUME.width* AssetsLoader.volume(), VOLUME.y+5 , 7);

    public static void draw(ShapeRenderer render ){
        render.begin(ShapeRenderer.ShapeType.Filled);
        render.setColor(Color.BLACK);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);

        render.setColor(Color.FIREBRICK);
        render.rect(VOLUME.x, VOLUME.y, VOLUME.width, VOLUME.height);

        render.setColor(Color.DARK_GRAY);
        render.rect(SLIDER.x, VOLUME.y, VOLUME.width - (SLIDER.x - VOLUME.x), VOLUME.height);

        render.end();

        render.begin(ShapeRenderer.ShapeType.Line);
        render.setColor(Color.RED);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);
        render.setColor(Color.WHITE);
        render.rect(VOLUME.x, VOLUME.y, VOLUME.width, VOLUME.height);

        render.end();

        render.begin(ShapeRenderer.ShapeType.Filled);
        render.setColor(dragging ? Color.ORANGE : Color.RED);
        render.circle(SLIDER.x, SLIDER.y, SLIDER.radius);
        render.end();
        render.begin(ShapeRenderer.ShapeType.Line);
        render.setColor(Color.YELLOW);
        render.circle(SLIDER.x, SLIDER.y, SLIDER.radius);
        render.end();

    }

    public static Result click(float x , float y){
        if(!DIALOG.contains(x,y)) return Result.OUT;
        if(SLIDER.contains(x,y)) return Result.SLIDER;
        if(VOLUME.contains(x,y)) {
            drag(x,y);
            return Result.VOLUME;
        }
        return Result.NULL;
    }

    public static void drag(float x , float y){
        if(x >= VOLUME.x && x <= VOLUME.x+VOLUME.width) {
            SLIDER.x = x;
            AssetsLoader.setVolume(computeVolume());
        } else if (x < VOLUME.x) {
            SLIDER.x = VOLUME.x;
            AssetsLoader.setVolume(computeVolume());
        } else if (x > VOLUME.x + VOLUME.width) {
            SLIDER.x = VOLUME.x + VOLUME.width;
            AssetsLoader.setVolume(computeVolume());
        }


    }

    private static float computeVolume(){
        return (SLIDER.x - VOLUME.x) / VOLUME.width;
    }

    public static boolean isDragging() {
        return dragging;
    }

    public static void setDragging(boolean dragging) {
        Options.dragging = dragging;
    }
}
