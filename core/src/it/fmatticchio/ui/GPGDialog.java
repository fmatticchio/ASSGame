package it.fmatticchio.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;

import it.fmatticchio.game.StringsHandler;
import it.fmatticchio.screen.GameScreen;

/**
 * Static class that handles the Google Play Games Dialog
 */
public class GPGDialog {
    public enum Result{
        ACHIEVEMENTS, LEADERBOARDS , SIGNIN , OUT , NULL
    }

    private static Rectangle
            DIALOG = new Rectangle(GameScreen.SCREENX/4f , GameScreen.SCREENY/5f , GameScreen.SCREENX/2f , 3*GameScreen.SCREENY/5f ) ,
            LEADERBOARD = new Rectangle(DIALOG.x + 10 , DIALOG.y +10 , DIALOG.width - 20 , 20),
            ACHIEVEMENTS = new Rectangle(DIALOG.x + 10 , DIALOG.y +35 , DIALOG.width - 20 , 20),
            SIGNIN = new Rectangle(DIALOG.x + 10 , DIALOG.y +60 , DIALOG.width - 20 , 20);

    public static void draw(ShapeRenderer render , Batch batcher ,  BitmapFont font , boolean isSignIn){
        String text = (isSignIn? StringsHandler.getString(StringsHandler.ISSIGNIN) : StringsHandler.getString(StringsHandler.ISSIGNOUT));
        render.begin(ShapeRenderer.ShapeType.Filled);
        render.setColor(Color.BLACK);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);
        render.setColor(Color.DARK_GRAY);
        render.rect(SIGNIN.x, SIGNIN.y, SIGNIN.width, SIGNIN.height);
        render.rect(ACHIEVEMENTS.x, ACHIEVEMENTS.y, ACHIEVEMENTS.width, ACHIEVEMENTS.height);
        render.rect(LEADERBOARD.x, LEADERBOARD.y, LEADERBOARD.width, LEADERBOARD.height);
        render.end();

        render.begin(ShapeRenderer.ShapeType.Line);
        render.setColor(Color.RED);
        render.rect(DIALOG.x, DIALOG.y, DIALOG.width, DIALOG.height);
        render.setColor(Color.WHITE);
        render.rect(SIGNIN.x, SIGNIN.y, SIGNIN.width, SIGNIN.height);
        render.rect(ACHIEVEMENTS.x, ACHIEVEMENTS.y, ACHIEVEMENTS.width, ACHIEVEMENTS.height);
        render.rect(LEADERBOARD.x, LEADERBOARD.y, LEADERBOARD.width, LEADERBOARD.height);
        render.end();

        batcher.begin();
        font.draw(batcher, text, DIALOG.x + 20, DIALOG.y+DIALOG.height - 15, DIALOG.width - 40, Align.left, true);

        font.draw(batcher, (isSignIn ? StringsHandler.getString(StringsHandler.SIGNOUT) : StringsHandler.getString(StringsHandler.SIGNIN) ), SIGNIN.x+15, SIGNIN.y + 15);

        if(!isSignIn) font.setColor(Color.GRAY);
        //TODO handle the strings
        font.draw(batcher, "Achievemets", ACHIEVEMENTS.x+15, ACHIEVEMENTS.y + 15);
        font.draw(batcher, "Leaderboard", LEADERBOARD.x+15, LEADERBOARD.y + 15);
        font.setColor(Color.WHITE);
        batcher.end();

    }

    public static Result click(float x , float y){
        if(!DIALOG.contains(x,y)) return Result.OUT;
        if(SIGNIN.contains(x,y)) return Result.SIGNIN;
        if(ACHIEVEMENTS.contains(x,y)) return Result.ACHIEVEMENTS;
        if(LEADERBOARD.contains(x,y)) return Result.LEADERBOARDS;
        return Result.NULL;
    }
}
