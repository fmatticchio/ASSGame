package it.fmatticchio.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import it.fmatticchio.game.ASSGame;
import it.fmatticchio.game.DesktopGS;

public class DesktopLauncher {
    public DesktopLauncher() {
    }

    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "ASS";
        config.width = 816;
        config.height = 544;
        new LwjglApplication(new ASSGame(new DesktopGS()), config);
    }
}