package it.fmatticchio.game;

import android.content.Intent;
import android.graphics.Color;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;

import it.fmatticchio.helpers.AchievementsHandler;

public class AndroidLauncher extends AndroidApplication implements IGoogleServices {
    private final static int REQUEST_CODE_UNUSED = 9002;
    private GameHelper _gameHelper;

    protected AdView adView;
    protected View gameView;

    private ASSGame game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        // Create the GameHelper.
        _gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
        _gameHelper.enableDebugLog(true);
        GameHelper.GameHelperListener gameHelperListener = new GameHelper.GameHelperListener() {
            @Override
            public void onSignInFailed() {

            }

            @Override
            public void onSignInSucceeded() {

            }
        };

        _gameHelper.setMaxAutoSignInAttempts(1); //try to sign in automatically

        _gameHelper.setup(gameHelperListener);

        //initialize(new ASSGame(this), config);  // manually to have ads

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        RelativeLayout layout = new RelativeLayout(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params);


        AdView admobView = createAdView();
        View gameView = createGameView(config);

        layout.addView(admobView);
        layout.addView(gameView);

        setContentView(layout);
        startAdvertising(admobView);

    }

    private AdView createAdView() {
        adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(getString(R.string.banner_ad_unit_id));
        adView.setId(R.id.adViewId);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        adView.setLayoutParams(params);
        adView.setBackgroundColor(Color.BLACK);
        return adView;
    }

    private View createGameView(AndroidApplicationConfiguration cfg) {
        StringsHandler.setStrings(getResources().getStringArray(R.array.strings));
        StringsHandler.setAchievement(getResources().getStringArray(R.array.achievements));
        game = new ASSGame(this);
        gameView = initializeForView(game, cfg);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.BELOW, adView.getId());
        gameView.setLayoutParams(params);
        return gameView;
    }

    private void startAdvertising(AdView adView) {
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        adView.loadAd(adRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) adView.resume();
    }

    @Override
    public void onPause() {
        if (adView != null) adView.pause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (adView != null) adView.destroy();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        _gameHelper.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        _gameHelper.onStop();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        _gameHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void signIn() {
        try {
            runOnUiThread(new Runnable() {
                //@Override
                public void run() {
                    _gameHelper.beginUserInitiatedSignIn();
                }
            });
            AchievementsHandler.sync();
        } catch (Exception e) {
            Gdx.app.log("MainActivity", "Log in failed: " + e.getMessage() + ".");
        }

    }

    @Override
    public void signOut() {

        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    _gameHelper.signOut();
                }
            });
        } catch (Exception e) {
            Gdx.app.log("MainActivity", "Log out failed: " + e.getMessage() + ".");
        }
    }

    @Override
    public void shareGame() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getText(R.string.share)+ " https://play.google.com/store/apps/details?id=it.fmatticchio.game");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    @Override
    public void rateGame(){

        String str = "https://play.google.com/store/apps/details?id=it.fmatticchio.game";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(str)));

    }

    @Override
    public void shareScore(long score) {

        String s = String.format(getResources().getText(R.string.shareScore) + " https://play.google.com/store/apps/details?id=it.fmatticchio.game", score);

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, s);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));

    }

    @Override
    public void submitScore(long score) {
        if (isSignedIn()) {
            Games.Leaderboards.submitScore(_gameHelper.getApiClient(), getString(R.string.leaderboard_id), score);
        }
    }

    @Override
    public void showScores() {

        if (isSignedIn()) {
            startActivityForResult(Games.Leaderboards.getLeaderboardIntent(_gameHelper.getApiClient(), getString(R.string.leaderboard_id)), REQUEST_CODE_UNUSED);
        }
    }

    //test
    @Override
    public boolean isSignedIn() {
        return _gameHelper.isSignedIn();
    }

    @Override
    public void showAchievement() {

        if (isSignedIn()) {
            startActivityForResult(Games.Achievements.getAchievementsIntent(_gameHelper.getApiClient()), REQUEST_CODE_UNUSED);
        }
    }

    @Override
    public void unlock(String achievement) {
        if(isSignedIn()){
            Games.Achievements.unlock(_gameHelper.getApiClient() , achievement);
        }
    }

    @Override
    public void onBackPressed() {
        //Implemented within the game
         /*AlertDialog dialog;
		 AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

		 builder.setMessage("Do you really want to quit?");

		 builder.setPositiveButton("QUIT", new DialogInterface.OnClickListener() {
			 @Override
			 public void onClick(DialogInterface dialog, int which) {
				finish();
			 }
		 });


		 builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			 @Override
			 public void onClick(DialogInterface dialog, int which) {
				// dialog.dismiss();
			 }
		 });

		 dialog = builder.create();


		 dialog.show();*/
    }



}
